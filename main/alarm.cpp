#include <atomic>
#include "driver/gpio.h"
#include "esp_http_server.h"
#include "esp_log.h"
#include "esp_mac.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "motion.hpp" // AM312 sensor
#include "nvs_flash.h"
#include "sdkconfig.h"

#define BLINK_GPIO GPIO_NUM_2
#define SIREN_GPIO GPIO_NUM_5

QueueHandle_t siren_queue;
QueueHandle_t blink_queue;
QueueHandle_t alarm_queue;
std::atomic<bool> enabling_alarm(false);
std::atomic<bool> alarm_enabled(false);
std::atomic<unsigned> auto_on_timer(0);

static esp_err_t status_get_handler(httpd_req_t *req)
{
	ESP_LOGD("webserver", "status handler");

	if (enabling_alarm) {
		const char *resp_str = "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/><style>body { background-color: #212121; color: white; font-family: verdana; font-size: 600%;} a { color: white;}</style></head><body><div>Statut: Activation...<br><br><a href=\"./enable\">Activer</a><br><br><a href=\"./disable\">Désactiver</a></div></body></html>";
		httpd_resp_send(req, resp_str, strlen(resp_str));
	} else if (!alarm_enabled) {
		const char *resp_str = "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/><style>body { background-color: #212121; color: white; font-family: verdana; font-size: 600%;} a { color: white;}</style></head><body><div>Statut: Désactivée<br><br><a href=\"./enable\">Activer</a><br><br><a href=\"./disable\">Désactiver</a></div></body></html>";
		httpd_resp_send(req, resp_str, strlen(resp_str));
	} else { // Enabled
		const char *resp_str = (const char *) req->user_ctx;
		httpd_resp_send(req, resp_str, strlen(resp_str));
	}

	return ESP_OK;
}

static const httpd_uri_t status = {
	.uri = "/",
	.method = HTTP_GET,
	.handler = status_get_handler,
	.user_ctx = (void *) "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/><style>body { background-color: #212121; color: white; font-family: verdana; font-size: 600%;} a { color: white;}</style></head><body><div>Statut: Activée<br><br><a href=\"./enable\">Activer</a><br><br><a href=\"./disable\">Désactiver</a></div></body></html>"
};

static esp_err_t disable_get_handler(httpd_req_t *req)
{
	ESP_LOGD("webserver", "disable handler");
	const char *resp_str = (const char *) req->user_ctx;
	httpd_resp_send(req, resp_str, strlen(resp_str));
	auto_on_timer = 0; // Reset auto-on timer
	const bool enabled(false);
	xQueueSendToBack(alarm_queue, &enabled, pdMS_TO_TICKS(5));
	return ESP_OK;
}

static const httpd_uri_t disable = {
	.uri = "/disable",
	.method = HTTP_GET,
	.handler = disable_get_handler,
	.user_ctx = (void *) "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/><meta http-equiv=\"refresh\" content=\"2;URL=..\"><style>body { background-color: #212121; color: white; font-family: verdana; font-size: 600%;} a { color: white;}</style></head><body><div>Alarme désactivée</div></body></html>"
};

static esp_err_t enable_get_handler(httpd_req_t *req)
{
	ESP_LOGD("webserver", "enable handler");

	if (enabling_alarm || alarm_enabled) {
		const char *resp_str = "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/><meta http-equiv=\"refresh\" content=\"2;URL=..\"><style>body { background-color: #212121; color: white; font-family: verdana; font-size: 600%;} a { color: white;}</style></head><body><div>Alarme déjà activée</div></body></html>";
		httpd_resp_send(req, resp_str, strlen(resp_str));
	} else {
		const char *resp_str = (const char *) req->user_ctx;
		httpd_resp_send(req, resp_str, strlen(resp_str));
		bool enabled(true);
		xQueueSendToBack(alarm_queue, &enabled, pdMS_TO_TICKS(5));
	}

	return ESP_OK;
}

static const httpd_uri_t enable = {
	.uri = "/enable",
	.method = HTTP_GET,
	.handler = enable_get_handler,
	.user_ctx = (void *) "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/><meta http-equiv=\"refresh\" content=\"2;URL=..\"><style>body { background-color: #212121; color: white; font-family: verdana; font-size: 600%;} a { color: white;}</style></head><body><div>Activation de l'alarme...</div></body></html>"
};

static httpd_handle_t start_webserver()
{
	httpd_handle_t server = NULL;
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();

	// Start the httpd server
	ESP_LOGI("webserver", "Starting server on port: '%d'", config.server_port);
	if (httpd_start(&server, &config) == ESP_OK) {
		// Set URI handlers
		ESP_LOGI("webserver", "Registering URI handlers");
		httpd_register_uri_handler(server, &status);
		httpd_register_uri_handler(server, &disable);
		httpd_register_uri_handler(server, &enable);
		return server;
	}

	ESP_LOGI("webserver", "Error starting server!");
	return NULL;
}

static void wifi_event_handler(void *arg,
							   esp_event_base_t event_base,
							   int32_t event_id,
							   void *event_data)
{
	if (event_id == WIFI_EVENT_AP_STACONNECTED) {
		wifi_event_ap_staconnected_t *event = (wifi_event_ap_staconnected_t *) event_data;
		ESP_LOGI("wifi", "station " MACSTR " join, AID=%d",
				 MAC2STR(event->mac), event->aid);
	} else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
		wifi_event_ap_stadisconnected_t *event = (wifi_event_ap_stadisconnected_t *) event_data;
		ESP_LOGI("wifi", "station " MACSTR " leave, AID=%d",
				 MAC2STR(event->mac), event->aid);
	}
}

void wifi_init_softap(void)
{
	ESP_ERROR_CHECK(esp_netif_init());
	ESP_ERROR_CHECK(esp_event_loop_create_default());
	esp_netif_create_default_wifi_ap();

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
					ESP_EVENT_ANY_ID,
					&wifi_event_handler,
					NULL,
					NULL));

	wifi_config_t wifi_config;
	memcpy(wifi_config.ap.ssid, CONFIG_WIFI_SSID, sizeof(CONFIG_WIFI_SSID));
	wifi_config.ap.ssid_len = strlen(CONFIG_WIFI_SSID);
	wifi_config.ap.channel = CONFIG_WIFI_CHANNEL;
	memcpy(wifi_config.ap.password, CONFIG_WIFI_PASSWORD, sizeof(CONFIG_WIFI_PASSWORD));
	wifi_config.ap.max_connection = 4;
	wifi_config.ap.authmode = WIFI_AUTH_WPA2_PSK;
	wifi_config.ap.pmf_cfg.required = true;

	if (strlen(CONFIG_WIFI_PASSWORD) == 0) {
		wifi_config.ap.authmode = WIFI_AUTH_OPEN;
	}

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
	ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());

	ESP_LOGI("wifi", "wifi_init_softap finished. SSID:%s password:%s",
			 CONFIG_WIFI_SSID, CONFIG_WIFI_PASSWORD);
}

static void task_siren(void *)
{
	bool state;
	while (1) {
		if (pdFAIL == xQueueReceive(siren_queue, &state, portMAX_DELAY))
			continue;
		ESP_LOGE("siren", "Start (intruder)");
		gpio_set_level(SIREN_GPIO, true);
		for (std::size_t i(0); i < CONFIG_SIREN_ON_DURATION; ++i) {
			if (!alarm_enabled)
				break;
			vTaskDelay(pdMS_TO_TICKS(1000));
		}
		gpio_set_level(SIREN_GPIO, false);
		xQueueReset(siren_queue);
		ESP_LOGW("siren", "Stopped");
	}
}

static void task_motion(void *)
{
	bool state;
	unsigned duration(100);
	while (1) {
		if (pdFAIL == xQueueReceive(motion_queue, &state, portMAX_DELAY))
			continue;

		auto_on_timer = 0; // Reset auto-on timer
		xQueueSendToBack(blink_queue, &duration, pdMS_TO_TICKS(5));

		if (state && alarm_enabled)
			xQueueSendToBack(siren_queue, &state, pdMS_TO_TICKS(5));
		if (state && !alarm_enabled)
			ESP_LOGI("motion", "Ignoring motion, alarm is disabled.");
	}
}

static void task_blink(void *)
{
	unsigned duration_ms(1);
	while (1) {
		if (pdFAIL == xQueueReceive(blink_queue, &duration_ms, portMAX_DELAY))
			continue;

		ESP_LOGD("blink", "%u ms", duration_ms);
		gpio_set_level(BLINK_GPIO, !alarm_enabled);
		vTaskDelay(pdMS_TO_TICKS(duration_ms));
		gpio_set_level(BLINK_GPIO, alarm_enabled);
	}
}

void task_alarm(void *)
{
	bool enable(false);
	while (1) {
		if (pdFAIL == xQueueReceive(alarm_queue, &enable, portMAX_DELAY))
			continue;

		if (enable) {
			enabling_alarm = true;
			ESP_LOGI("alarm", "Enabling in %d seconds...", CONFIG_ALARM_TIME_BEFORE_ON);
			vTaskDelay(pdMS_TO_TICKS(CONFIG_ALARM_TIME_BEFORE_ON * 1000));
			ESP_LOGW("alarm", "Enabled");
		} else {
			ESP_LOGW("alarm", "Disabled");
		}

		gpio_set_level(BLINK_GPIO, enable);
		xQueueReset(siren_queue);
		alarm_enabled = enable;
		enabling_alarm = false;
	}
}

void task_auto_enable(void *)
{
	while (1) {
		vTaskDelay(pdMS_TO_TICKS(60000)); // 1 minute

		if (enabling_alarm || alarm_enabled)
			continue;

		++auto_on_timer;
		ESP_LOGI("auto_enable", "Timer %d. Auto on after %d", auto_on_timer.load(), CONFIG_SIREN_AUTO_ON_DELAY);

		if (auto_on_timer >= CONFIG_SIREN_AUTO_ON_DELAY) {
			if (enabling_alarm || alarm_enabled)
				continue;

			ESP_LOGI("auto_enable", "Auto enabling alarm");
			bool enabled(true);
			xQueueSendToBack(alarm_queue, &enabled, pdMS_TO_TICKS(5));
		}
	}
}

extern "C" void app_main()
{
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	ESP_ERROR_CHECK(esp_netif_init());
	wifi_init_softap();

	esp_rom_gpio_pad_select_gpio(SIREN_GPIO);
	gpio_set_direction(SIREN_GPIO, GPIO_MODE_OUTPUT);
	gpio_set_level(SIREN_GPIO, false);
	esp_rom_gpio_pad_select_gpio(BLINK_GPIO);
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
	gpio_set_level(BLINK_GPIO, false);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
	static httpd_handle_t server = start_webserver();
#pragma GCC diagnostic pop

	siren_queue = xQueueCreate(100, sizeof(bool));
	xTaskCreate(task_siren, "siren", 4096, NULL, configMAX_PRIORITIES - 1, NULL);

	alarm_queue = xQueueCreate(100, sizeof(bool));
	xTaskCreate(task_alarm, "alarm", 4096, NULL, configMAX_PRIORITIES - 2, NULL);

	blink_queue = xQueueCreate(100, sizeof(unsigned));
	xTaskCreate(task_blink, "blink", 4096, NULL, configMAX_PRIORITIES - 2, NULL);

	motion_queue = xQueueCreate(100, sizeof(bool));
	xTaskCreate(task_motion, "motion", 4096, NULL, configMAX_PRIORITIES - 1, NULL);

	xTaskCreate(task_auto_enable, "auto_enable", 4096, NULL, configMAX_PRIORITIES - 1, NULL);

	motion_sensor_init();
}

