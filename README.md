# Dependencies
[Get ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html#step-2-get-esp-idf)

# Compiling
```bash
git clone https://gitlab.com/VictorLamoine/esp_idf_alarm.git
cd esp_idf_examples
get_idf # See https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html#id5
idf.py build
```

Use `idf.py menuconfig` to configure the project if needed.

# Using
Connect to the Wi-Fi broadcasted by the ESP32 and access one of these pages:
- `http://192.168.1.4`
- `http://192.168.1.4/enable`
- `http://192.168.1.4/disable`
